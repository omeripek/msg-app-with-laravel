<?php

namespace App\Http\Controllers;

use App\Http\Requests\MessageFormRequest;
use App\Messages;
use App\Chats;
use App\User;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\Eloquent\RelationNotFoundException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class MessagesController extends Controller
{

    /**
     * Display a listing of the Chat Threats.
     *
     * @return View
     */
    public function index()
    {

        $topics = Chats::all();

        return view('msgsend.index', compact('topics'));
    }


    /**
     * Display a listing of the chat history
     *
     * @return View
     */
    public function chatHistory()
    {

        $messages = Messages::all();

        return view('msgsend.list', compact('messages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create($id)
    {
        $user = User::find($id);
        return view('msgsend.create', compact('user'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
      //  $input = Input::all();

     //   $topic = Chats::create(


      //  );


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * For chat list
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function chats($id)
    {
        //
    }
}
