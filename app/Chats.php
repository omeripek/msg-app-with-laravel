<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chats extends Model
{
    protected $authUserId;
    protected $table = 'chats';



    /**
     * set authenticated user_id for global
     *
     * @param int $id
     *
     * @return int|bool
     */
    public function setAuthUserId($id = null)
    {
        if (!is_null($id)) {
            return $this->authUserId = $id;
        }
        return false;
    }

    public function user($id = null)
    {
        if ($this->setAuthUserId($id)) {
            return $this;
        }
        return false;
    }

    /**
     * Merge speakers with
     *
     */
    protected function getRangeUser($user1, $user2)
    {
        $user = [];
        $user['st'] = ($user1 < $user2) ? $user1 : $user2;
        $user['nd'] = ($user1 < $user2) ? $user2 : $user1;
        return $user;
    }

}
