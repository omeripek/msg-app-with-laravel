@extends('layouts.app')


@section('content')
    <div class="container">

        <ul>

            @foreach($messages as $list)
                @if(!is_null($list->title))
                    <li class="clearfix">
                        <a href="{{route('message.read', ['id'=>$list->withUser->id])}}">
                            <div class="about">
                                <div class="name">{{$list->withUser->name}}</div>
                                <div class="status">
                                    @if(auth()->user()->id == $list->thread->sender->id)
                                        <span class="fa fa-reply"></span>
                                    @endif
                                    <span>{{substr($list->thread->message, 0, 15)}}</span>
                                </div>
                            </div>
                        </a>
                    </li>
                @endif
            @endforeach

        </ul>

    </div>
@stop