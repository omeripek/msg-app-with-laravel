@extends('layouts.app')

@section('header')
    <nav>
        <ul class="nav nav-pills pull-right">
            <li role="presentation" class="active"><a href="{{ url('/') }}">Home</a></li>
            <li role="presentation"><a href="{{ url('/about') }}">About</a></li>
            <li role="presentation"><a href="{{ url('/contact') }}">Contact</a></li>
        </ul>
    </nav>
@stop

@section('sidebar-up')
    <h1>CONTACT US!</h1>
@stop

@section('sidebar-left')
    <h3>Contact Informations</h3>
    <p>abc [at] msapp</p>
    <p>025221525522 </p>
    <p>Adres: xxxxx yyyy zzzzzz ddddd vvvv</p>
    <p><b>Istanbul / TURKEY </b></p>
@stop
@section('sidebar-right')
    <div class="col-md-11 text-center">
        <h4><i class='glyphicon glyphicon-user'></i> Login Screen</h4>

    </div>
    <div class="alert-info">
      @if (Auth::guest())
        <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">

            {{ csrf_field() }}

            <div class="form-group">
                <label class="col-md-4 control-label">E-Mail Address</label>
                <div class="col-md-6">
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-4 control-label">Password</label>
                <div class="col-md-6">
                    <input type="password" class="form-control" name="password">
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        Login
                    </button>
                </div>
            </div>
        </form>
        <br />
      @else
          <div class="text-center">
        <div>
            Welcome,  {{ Auth::user()->name }}

        </div>
          <div>
              <ul>
                  <li>
                      <a href="{{ url('/logout') }}"
                         onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                          Logout
                      </a>

                      <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                          {{ csrf_field() }}
                      </form>
                  </li>
                  
              </ul>

              <ul>
                  <li>
                      <a href="{{ url('/profile') }}"> My Profiles</a>
                  </li>

                  <li>
                      <a href="{{ url('/users') }}"> Users</a>
                  </li>

                  <li>
                      <a href="{{ url('/messages') }}"> Messages</a>
                  </li>
              </ul>
          </div>
              <br />
          </div>

        @endif
    </div>
@stop
@section('footer')
    <p>&copy; 2016 MsgApp {{ url('/') }}</p>
@stop