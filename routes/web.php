<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/contact', function () {
    return view('contact');
});

Route::get('/profile', function () {
    return view('auth.profile');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::resource('/users', 'UserController');

//Route::get('/users', function () {
//    return view('users');
//});

//Route::get('/users', 'UserController@index');

//Route::resource('create', 'MessagesController');
Route::get('/messages', 'MessagesController@index');

Route::get('message/{id}', 'MessagesController@create');
Route::post('message/{id}', 'MessagesController@store');

Route::get('list/{id}', 'MessageController@chats')->name('message.read');

