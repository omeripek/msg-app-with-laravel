<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 10)->create()->each(function ($u){

        });
        /**
        DB::table('users')->insert([
            'username' => 'admin',
            'name' => 'admin',
            'surname' => str_random(15),
            'email' => 'admin@maillocalhost.co',
            'password' => bcrypt('admin'),
        ]);

        DB::table('users')->insert([
            'username' => 'test',
            'name' => 'demo',
            'surname' => 'user',
            'email' => str_random(10).'@maillocalhost.co',
            'password' => bcrypt('test'),
        ]);

         **/

    }
}
